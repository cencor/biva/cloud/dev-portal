# Búsqueda de series de Avisos de oferta pública

Devuelve series de los Avisos de oferta pública.

## Request

**GET** `/emisoras/avisos-oferta-publica/series`

## Response

```
[
  "*",
  "00219",
  "20-7",
  "20V",
  "21G",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/avisos-oferta-publica/series`

## Path variables

## Query parameters

## Atributos en la respuesta