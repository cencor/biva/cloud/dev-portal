# Avisos de oferta pública

Devuelve los avisos de oferta pública, convocatorias de subasta, resultado de subasta, cierre de libro y avisos con fines informativos, donde se notifican las características de la oferta.

## Request

**GET** `/emisoras/avisos-oferta-publica`

## Response

```
{
  "content": [
      {
      "id": 45170,
      "claveCotizacion": "COXA",
      "razonSocial": "Cox Energy América, S.A.B. de C.V.",
      "tipoValor": "ACCIONES DE EMPRESAS COMERCIALES, INDUSTRIALES Y DE SERVICIOS",
      "tipoAviso": "Inicio de oferta",
      "fechaPublicacion": 1593704438000,
      "urlDocumento": "/storage/data-listing/3869/0000831/17/1/Cox%20Energy%20%C2%A0Proyecto%20Galileo%20%20Aviso%20de%20Oferta%20Pública.pdf",
      "docType": "PDF",
      "series": [
        "II"
      ],
      ...
    }
  ],
  "number": 0,
  "size": 10,
  "totalElements": 2,
  "totalPages": 1
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/avisos-oferta-publica?size=10&page=0`

## Path variables

N/A

## Query parameters

* **claveCotizacion** Clave de cotización.
* **idTipoInstrumento** Identificador del tipo de instrumento.
* **serie** Serie.

## Atributos en la respuesta