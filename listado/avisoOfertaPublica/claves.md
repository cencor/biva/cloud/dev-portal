# Búsqueda de claves de cotización de Avisos de oferta pública

Devuelve claves de cotización de los Avisos de oferta pública.

## Request

**GET** `/emisoras/avisos-oferta-publica/claves-cotizacion`

## Response

```
[
  "AMZ106L",
  "BANOB",
  "FORION",
  "MONEX",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/avisos-oferta-publica/claves-cotizacion`

## Path variables

## Query parameters

## Atributos en la respuesta