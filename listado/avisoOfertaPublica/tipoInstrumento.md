# Búsqueda de tipos de instrumento de Avisos de oferta pública

Devuelve el catálogo de tipos de instrumento disponibles de los Avisos de oferta pública.

## Request

**GET** `/emisoras/avisos-oferta-publica/tipos-instrumento`

## Response

```
[
  {
    "id": 1,
    "nombre": "Acciones, CPOs, UVs"
  }
  {
    "id": 20,
    "nombre": "TRAC´s"
  },
  {
    "id": 5,
    "nombre": "Titulos Opcionales (Warrants)"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/avisos-oferta-publica/tipos-instrumento`

## Path variables

## Query parameters

## Atributos en la respuesta