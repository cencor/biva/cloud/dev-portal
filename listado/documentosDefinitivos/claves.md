# Búsqueda de claves de Documentos definitivos de listado

Devuelve claves de los Documentos definitivos de listado.

## Request

**GET** `/emisoras/documentos-definitivos/claves`

## Response

```
[
  "ALTUMCK",
  "ELEKTRA",
  "FIMUBCB",
  "SALUD",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/documentos-definitivos/claves`

## Path variables

## Query parameters

## Atributos en la respuesta