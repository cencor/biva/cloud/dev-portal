# Búsqueda de tipos de instrumento para Documentos definitivos de listado

Devuelve el catálogo de tipos de instrumento disponibles de los Documentos definitivos de listado.

## Request

**GET** `/emisoras/documentos-definitivos/tipos-instrumento`

## Response

```
[
  {
    "id": 3,
    "nombre": "CKDes y CERPIS"
  },
  {
    "id": 11,
    "nombre": "Instrumentos Bancarios Largo Plazo"
  },
  {
    "id": 4,
    "nombre": "Obligaciones"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/documentos-definitivos/tipos-instrumento`

## Path variables

## Query parameters

## Atributos en la respuesta