# Búsqueda de warrants de Documentos definitivos de listado

Devuelve claves warrant de los Documentos definitivos de listado.

## Request

**GET** `/emisoras/documentos-definitivos/warrants`

## Response

```
[
  "AMZ005L",
  "CMX008R",
  "NFX011L",
  "WYN104L",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/documentos-definitivos/warrants`

## Path variables

## Query parameters

## Atributos en la respuesta
