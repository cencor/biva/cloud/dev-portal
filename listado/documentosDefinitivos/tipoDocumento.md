# Tipos de documento de Documentos definitivos de listado

Devuelve el catálogo de los tipos de documento disponibles de Documentos definitivos de listado.

## Request

**GET** `emisoras/documentos-definitivos/tipos-documento`

## Response

```
[
  {
    "id": 32,
    "nombre": "Acreditación de uso de licencias de índices"
  }
  {
    "id": 71,
    "nombre": "Carta de autorización BIVA"
  },
  {
    "id": 75,
    "nombre": "Presentación Inversionistas"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/documentos-definitivos/tipos-documento`

## Path variables

## Query parameters

## Atributos en la respuesta