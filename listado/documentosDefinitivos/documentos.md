# Documentos definitivos de listados

Devuelve las versiones definitivas de los documentos una vez realizada la colocación de los valores.

## Request

**GET** `/emisoras/documentos-definitivos`

## Response

```
{
  "content": [
    {
      "id": 62915,
      "tipoDocumento": "Poder del representante legal de la emisora",
      "fechaPublicacion": 1633035688000,
      "nombreArchivo": "MOC1 - copia (10).pdf",
      "urlDocumento": "/storage/data-listing/10513/0001607/4/1/MOC1%20-%20copia%20(10).pdf",
      "docType": "PDF"
    }
    ...
  ],
  "number": 0,
  "size": 10,
  "totalElements": 5725,
  "totalPages": 573
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/documentos-definitivos?size=10&page=0`

## Path variables

N/A

## Query parameters

* **clave** Clave de emisora.
* **idTipoInstrumento** Identificador del tipo de instrumento.
* **idTipoDocumento** Identificador del tipo de documento.
* **claveWarrant** Clave de warrant.
* **serie** Serie.

## Atributos en la respuesta
