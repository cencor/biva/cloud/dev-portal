# Búsqueda de series de Documentos definitivos de listado

Devuelve series de los Documentos definitivos de listado.

## Request

**GET** `/emisoras/documentos-definitivos/series`

## Response

```
[
  "*",
  "01920",
  "19-2",
  "A",
  "DC063",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/documentos-definitivos/series`

## Path variables

## Query parameters

## Atributos en la respuesta