# Búsqueda de claves de Trámites de listado

Devuelve claves de los Trámites de listado.

## Request

**GET** `/emisoras/tramites/claves`

## Response

```
[
  "123LCB",
  "ARREACT",
  "FEFA",
  "FHIPOCB",
  "FNCOT",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/tramites/claves`

## Path variables

## Query parameters

## Atributos en la respuesta