# Búsqueda de tipos de valor de Trámites de listado

Devuelve el catálogo de tipos de valor de los Trámites de listado.

## Request

**GET** `/emisoras/tramites/tipos-valor`

## Response

```
[
  {
    "id": 65,
    "nombre": "CERTIFICADOS BURSÁTILES DE BANCA DE DESARROLLO"
  },
  {
    "id": 10,
    "nombre": "FIBRAS CERTIFICADOS INMOBILIARIOS"
  },
  {
    "id": 11,
    "nombre": "FIDEICOMISOS DE INVERSIÓN EN ENERGÍA (FIBRA E)"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/tramites/tipos-valor`

## Path variables

## Query parameters

## Atributos en la respuesta