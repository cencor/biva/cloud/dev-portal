# Tipos de documento de Trámite de listado

Devuelve el catálogo de los tipos de documento disponibles en un Trámite de listado.

## Request

**GET** `emisoras/tramites/{id}/documentos/tipos-documento`

## Response

```
[
  {
    "id": 20,
    "nombre": "Aviso con fines informativos (Oferta definitiva)"
  },
  {
    "id": 74,
    "nombre": "Documento suscrito por el auditor externo independiente"
  },
  {
    "id": 12,
    "nombre": "Opinión legal, en su caso versar sobre la constitución del fideicomiso y las garantías"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/tramites/1934/documentos/tipos-documento`

## Path variables

* **id**: Valor "id" del resultado del servicio de Trámites de listado del cual se quiere consultar sus documentos.

## Query parameters

## Atributos en la respuesta
