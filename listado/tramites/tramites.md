# Trámites de listado

Devuelve los trámites de listado de valores en proceso de revisión.

## Request

**GET** `emisoras/tramites`

## Response

```
{
  "content": [
    {
      "id": "1934",
      "clave": "VASCONI",
      "razonSocial": "GRUPO VASCONIA S.A.B.",
      "numeroTramite": "0000814",
      "documentosDisponibles": true,
      "fechaSolicitud": 1591051768000,
      "tipoTramite": "Programa/Emisión con colocaciones subsecuentes con oferta"
    },
    ...
  ],
  "number": 0,
  "size": 10,
  "totalElements": 166,
  "totalPages": 17
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/tramites?size=10&page=0`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **clave**: Clave de la emisora.
* **idTipoValor**: Identificador del tipo de valor.
* **fechaInicio**: La fecha de inicio en formato yyyy-MM-dd (p. ej. 2021-08-30).
* **fechaFin**: La fecha de fin en formato yyyy-MM-dd (p. ej. 2021-09-30).

## Atributos en la respuesta