# Documentos de Trámite de listado

Devuelve los documentos publicados de un Trámite de listado.

## Request

**GET** `emisoras/tramites/{id}/documentos`

## Response

```
{
  "content": [
    {
      "id": 49624,
      "numeroEnvio": 6,
      "nombreArchivo": "Oficio de Autorización CNBV.pdf",
      "tipoDocumento": "Oficio de la Comisión Nacional Bancaria y de Valores",
      "fechaRecepcion": 1606327152000,
      "urlDocumento": "/storage/data-listing/1649/0000814/30/1/Oficio%20de%20Autorización%20CNBV.pdf",
      "docType": "PDF"
    },
    ...
  ],
  "number": 0,
  "size": 10,
  "totalElements": 89,
  "totalPages": 9
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/tramites/1934/documentos?size=10&page=0`

## Path variables

* **id**: Valor "id" del resultado del servicio de Trámites de listado del cual se quiere consultar sus documentos.

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **idTipoDocumento**: Identificador del tipo de documento.
* **numeroEnvio**: Número de envío.

## Atributos en la respuesta