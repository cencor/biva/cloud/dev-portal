# Números de envío de documentación en Trámite de listado

Devuelve los números de envío de documentación realizados en un Trámite de listado.

## Request

**GET** `emisoras/tramites/{id}/documentos/numeros-envio`

## Response

```
[
  3,
  2,
  1,
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/tramites/1934/documentos/numeros-envio`

## Path variables

* **id**: Valor "id" del resultado del servicio de Trámites de listado del cual se quiere consultar sus documentos.

## Query parameters

## Atributos en la respuesta