# Prospectos de colocación

Documentos que contienen información de las emisoras.

## Request

**GET** `/emisoras/prospectos`

## Response

```
{
  "content": [
    {
      "id": 51226,
      "fechaPublicacion": 1608313207000,
      "tipoDocumento": "Prospecto de colocación o Folleto informativo",
      "descripcion": "PROGRAMA DE CERTIFICADOS BURSÁTILES DE LARGO PLAZO",
      "urlDocumento": "/storage/data-listing/1726/0000980/21/1/CADU-prosp10-21022018-100706-1.pdf",
      "docType": "PDF",
      "clave": "CADU",
      "razonSocial": "CORPOVAEL S.A.B. DE C.V."
    },
    ...
  ],
  "number": 4,
  "size": 10,
  "totalElements": 90,
  "totalPages": 9
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/prospectos?size=10&page=0`

## Path variables

N/A

## Query parameters

* **clave** Clave de la emisora.
* **idTipoMercado** Identificador del tipo de mercado.
* **idTipoInstrumento** Identificador del tipo de instrumento.

## Atributos en la respuesta