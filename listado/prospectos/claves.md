# Búsqueda de claves de Prospectos de colocación

Devuelve claves de los Prospectos de colocación.

## Request

**GET** `/emisoras/prospectos/claves`

## Response

```
[
  "MEXAMX",
  "MONEX",
  "SVPI",
  "TRAXION",
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/prospectos/claves`

## Path variables

## Query parameters

## Atributos en la respuesta
