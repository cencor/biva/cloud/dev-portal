# Suplementos, DICIs y anexos asociados a Prospecto de colocación

Devuelve los Suplementos, DICIs, y anexos asociados a un Prospecto de colocación.

## Request

**GET** `emisoras/prospectos/{id}/documentos`

## Response

```
{
  "content": [
    {
      "id": 51230,
      "fechaPublicacion": 1608313350000,
      "tipoDocumento": "Prospecto de colocación o Folleto informativo",
      "descripcion": "Aviso de Actualización al prospecto",
      "urlDocumento": "/storage/data-listing/1726/0000980/21/2/CADU-%20Bono%20Verde-%20Aviso%20con%20Fines%20Informativos%20(Incremento%20en%20el%20Programa).pdf",
      "docType": "PDF"
    },
    {
      "id": 51236,
      "fechaPublicacion": 1608313531000,
      "tipoDocumento": "Documento con información clave para la inversión (DICI)",
      "descripcion": "EMISIÓN CADU 20V",
      "urlDocumento": "/storage/data-listing/1726/0000980_0001035/23/1/CADU-%20Bono%20Verde-%20DICI.pdf",
      "docType": "PDF",
      "series": [
        "20V"
      ]
    },
    ...
  ],
  "number": 0,
  "size": 10,
  "totalElements": 3,
  "totalPages": 1
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/prospectos/61134/documentos?size=10&page=0`

## Path variables

* **id**: Valor "id" del resultado del servicio de Prospectos de colocación del cual se quiere consultar sus Suplementos, DICIs y anexos.

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**

## Atributos en la respuesta