# Búsqueda de tipos de instrumento de Prospectos de colocación

Devuelve el catálogo de tipos de instrumento disponibles de los Prospectos de colocación.

## Request

**GET** `/emisoras/prospectos/tipos-instrumento`

## Response

```
[
  {
    "id": 8,
    "nombre": "Certificado Bursátil Corto Plazo"
  },
  {
    "id": 10,
    "nombre": "Certificado Bursátil de Entidades del Gobierno Federal"
  },
  {
    "id": 2,
    "nombre": "FIBRAS"
  },
  ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/prospectos/tipos-instrumento`

## Path variables

## Query parameters

## Atributos en la respuesta