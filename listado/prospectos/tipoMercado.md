# Búsqueda de tipos de mercado de Prospectos de colocación

Devuelve el catálogo de tipos de mercado de los Prospectos de colocación.

## Request

**GET** `/emisoras/prospectos/tipos-mercado`

## Response

```
[
  {
    "id": "3,4",
    "nombre": "SIC BIVA"
  },
  {
    "id": "1",
    "nombre": "Capitales"
  },
  {
    "id": "2",
    "nombre": "Deuda"
  }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/prospectos/tipos-mercado`

## Path variables

## Query parameters

## Atributos en la respuesta