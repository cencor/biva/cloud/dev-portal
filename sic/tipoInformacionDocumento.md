# Tipos de información de los documentos que se han enviado

Devuelve el nombre y id de los tipos de información de los documentos que las empresas han enviado.

## Request

**GET** `/emisoras/banco-informacion/tipo-informacion`

## Response

```
[
  {
    "id": "21",
    "nombre": "Aviso de extemporaneidad",
    "tipo": "mantenimiento"
  },
  {
    "id": "8",
    "nombre": "Eventos relevantes",
    "tipo": "mantenimiento"
  },
  {
    "id": "15",
    "nombre": "Información anual",
    "tipo": "mantenimiento"
  },
  {
    "id": "16",
    "nombre": "Información mensual",
    "tipo": "mantenimiento"
  },
  {
    "id": "10",
    "nombre": "Jurídico corporativo",
    "tipo": "mantenimiento"
  }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/banco-informacion/tipo-informacion?empresa=2201`

## Path variables

N/A

## Query parameters

* **empresa**: El identificador de la empresa que envió el documento.

## Atributos en la respuesta