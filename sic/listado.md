# Listado de emisoras SIC

Devuelve las emisoras SIC registradas.

## Request

**GET** `/emisoras/sic`

## Response

```
{
    "content": [
        {
            "id": 597,
            "clave": "0JN9",
            "nombre": "DSV PANALPINA A/S",
            "estatus": "Activa"
        },
        ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 2776,
    "totalPages": 278
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sic?size=10&page=0&tipoInstrumento=18&mercadoPrincipal=712`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **tipoInstrumento** El identificador del tipo de instrumento. Posibles valores `17` = Capitales, `18` ETFs, `19` = Deuda
* **mercadoPrincipal** El identificador del mercado principal.
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta