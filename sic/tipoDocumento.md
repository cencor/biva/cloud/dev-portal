# Tipos de documento de los documentos que se han enviado

Devuelve el nombre y id de los tipos de documento de los documentos que las empresas han enviado.

## Request

**GET** `/emisoras/banco-informacion/tipo-documento`

## Response

```
[
  {
    "id": "44,147",
    "nombre": "Anexo T",
    "tipo": "mantenimiento"
  },
  {
    "id": "45",
    "nombre": "Constancia mensual",
    "tipo": "mantenimiento"
  }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/banco-informacion/tipo-documento?empresa=2201&tipoInformacion=16`

## Path variables

N/A

## Query parameters

* **empresa**: El identificador de la empresa que envió el documento.
* **tipoInformacion**: El identificador del tipo de información

## Atributos en la respuesta