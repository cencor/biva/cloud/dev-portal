# Mercado principal de emisoras SIC

Devuelve los mercados principales de emisoras SIC.

## Request

**GET** `/emisoras/sic/mercado?&tipoInstrumento=19`

## Response

```
[
    {
        "id": 123,
        "codigoMercado": "XBER",
        "nombre": "BOERSE BERLIN",
        "pais": 85
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sic/mercado?tipoInstrumento=19`

## Path variables

N/A

## Query parameters

* **tipoInstrumento** El identificador del tipo de instrumento. Posibles valores `17` = Capitales, `18` ETFs, `19` = Deuda
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta