# Búsqueda de emisoras SIC

Busca emisoras SIC por su clave.

## Request

**GET** `emisoras/sic/busqueda/{clave}`

## Response

```
[
    {
        "id": 3040,
        "clave": "BA1"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/empresas/busqueda/BA1`

## Path variables

* **clave**: Clave de la emisora a buscar, permite desde un solo carácter.

## Query parameters

N/A

## Atributos en la respuesta