# Empresas que han enviado documentos

Devuelve el nombre y id de las empresas que han enviado documentos.

## Request

**GET** `/emisoras/banco-informacion/claves`

## Response

```
[
  {
    "id": 3229,
    "clave": "123LCB"
  },
  {
    "id": 1798,
    "clave": "AA1CK"
  }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/banco-informacion/claves?tipoInformacion=16&tipoDocumento=44,147`

## Path variables

N/A

## Query parameters

* **tipoInformacion**: El identificador del tipo de información
* **tipoDocumento** El identificador del tipo de documento
* **tipoInstrumento** El identificador del tipo de instrumento

## Atributos en la respuesta