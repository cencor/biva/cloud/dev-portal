# Documentos enviados

Devuelve los documentos que las emisoras han enviado.

## Request

**GET** `/emisoras/banco-informacion`

## Response

```
{
  "content": [
    {
      "id": 19958,
      "idDocumento": 19958,
      "clave": "CS2CK",
      "fechaPublicacion": 1607726229000,
      "docType": "PDF",
      "tipoDocumento": "Acuerdos de asamblea Tenedores de títulos 10/12/2020",
      "nombreArchivo": "/storage/data-maintenance/biva_20672.pdf"
    }
  ],
  "number": 0,
  "size": 1,
  "totalElements": 6921,
  "totalPages": 6921
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/banco-informacion?size=1&page=0`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **tipoInformacion**: El identificador del tipo de información
* **tipoDocumento** El identificador del tipo de documento
* **tipoInstrumento** El identificador del tipo de instrumento
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd
* **empresa** El identificador de la empresa
* **periodo** El periodo al que pertenece el documento. Formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
* **ejercicio** El año al que pertenece el documento en formato yyyy

## Atributos en la respuesta

* **idDocumento** El identificador del documento.
* **clave** La clave de la emisora que envió el documento.
* **fechaPublicacion** La fecha de publicación del documento en formato EPOCH.
* **docType** La extensión del documento
* **tipoDocumento** La descripción del tipo de documento
* **nombreArchivo** La ruta relativa a https://www.biva.mx donde se encuentra el documento.
