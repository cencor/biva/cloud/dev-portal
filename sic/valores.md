# Valores listados de la emisora SIC especificada

Presenta información de los valores listados de la emisora SIC especificada.

## Request

**GET** `/emisoras/sic/{id_emisora}/emisiones`

## Response

```
{
    "content": [
        {
            "id": 6805,
            "serie": "N",
            "isin": "KYG9830T1067",
            "estatus": "Activo",
            "mercado": "XHKG - HONG KONG EXCHANGES AND CLEARING LTD",
            "pais": "KY - Islas Caimán",
            "listado": "Registro Secundario",
            "tipoInstrumento": "SIC Acciones",
            "fechaListadoBIVA": 1608184800000
        }
    ],
    "number": 0,
    "size": 10,
    "totalElements": 1,
    "totalPages": 1
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sic/6175/emisiones?size=10&page=0`

## Path variables

* **id_emisora** El identificador de la emisora

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**

## Atributos en la respuesta