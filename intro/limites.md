# Límites de solicitudes.

BIVA Cloud cuenta con algunas restricciones de uso con fines de asegurar un uso equilibrado entre todos nuestros clientes. Además estas restricciones nos permiten asegurar la estabilidad de nuestros servicios.

Nuestros límites de solicitudes son **100 peticiones por segundo**, permitiendo ráfagas (burst) de 50 peticiones y las restantes distribuídas en el segundo.

## Sandbox

Nuestro ambiente de pruebas cuenta con restricciones mayores, quedando en 10 peticiones por segundo, permitiendo ráfagas de 5 y las restantes distribuídas en el segundo.