# Versionamiento de APIs

En BIVA Cloud sabemos la importancia que tiene cada contrato API para ti que lo consumes. Por esta razón cada vez que realizamos un cambio a un API lo diseñamos siempre teniendo en cuenta que sean `backwards-compatible`. Por ejemplo:

* Agregando nuevos atributos a la respuesta
* Agregando nuevos endpoints
* Agregando nuevos métodos a un endpoint ya existente
* Agregando nuevos query parameters

De tal forma que tu cliente del API no se vea afectado por estos cambios.

Para los casos donde el recurso devuelto por el API cambie de forma que no sea compatible hacia atrás, nosotros mantendremos la versión actual y la nueva, cada una en diferentes endpoints de tal forma que tu cliente REST nunca sea afectado.
