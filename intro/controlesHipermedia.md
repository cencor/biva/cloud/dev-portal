## Controles hipermedia

Algunas de nuestras APIs cuentan con [controles hipermedia](https://martinfowler.com/articles/richardsonMaturityModel.html#level3) que te ayudarán con información adicional relacionada al recurso que solicitaste. 

Por ejemplo, existen APIs que por la cantidad de información disponible, te devuelven la información paginada. En estos casos la respuesta contendrá, además del recurso solicitado, controles hipermedia que te ayudarán a determinar información de la página recibida como número de página, número de elementos por página, etc. además de enlaces relacionados a la página como el enlace para obtener la siguiente página, la anterior, la última.
