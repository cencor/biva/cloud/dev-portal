# Autenticación

BIVA Cloud autentica tus solicitudes utilizando API Tokens. Para utilizar cualquier API de BIVA Cloud, deberás proporcionar un API Token en cada solicitud. Si no incluyes tu API Token al realizar alguna solicitud a un API, o usas alguna incorrecta o que haya sido deshabilitada, BIVA Cloud te devolverá un error indicando que no tienes permisos.

Será necesario que se agregue el header `x-api-key` que incluya el valor de tu API Key. El API Key es similar a `bmPaCnr1sB1yLEWdBAup25BMY3xz5P4ntRVF`

BIVA Cloud proporciona un API Token secreto que debe ser mantenido en confidencialidad y solo utilizado en tus propios servidores o en algún lugar seguro que te permita utilizarlo en nuestro plugin de Microsoft Excel. Este API Token te permitirá ejecutar cualquier API dentro de BIVA Cloud.

## Protege tus API Tokens

* **Mantenlos seguros.** Tu API Token puede realizar cualquier llamado a nuestras APIs. Además el API Token es utilizado para determinar su uso y su correspondiente facturación. Te recomendamos que este API Token no lo guardes nunca en tu sistema de control de versiones. Especialmente si usas Git, no lo guardes ahí pues se quedará por siempre en tu repositorio. Alguien con acceso a tu repositorio puede tener acceso a ese API Token. Te recomendamos que no utilices el API Token fuera de tus servidores donde se llevan a cabo las consultas. Si lo vas a utilizar con nuestro plugin para Microsoft Excel, almacena tu API Token en algún lugar seguro. Te recomendamos utilizar un password manager y almacenarlo ahí.

* **No coloques tu API Key directamente en el código** En lugar de __hardcodear__ tu API Key dentro de tu código, mejor colócalo como variable de ambiente o en bóvedas.

* **Utiliza API Keys diferentes para diferentes apps** De esta forma se delimita el alcance de cada API Key. Además si un API Key está comprometido, puedes reemplazarlo sin impactar el resto de API Keys.
