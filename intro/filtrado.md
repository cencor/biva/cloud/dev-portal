# Filtrado de resultados.

Nuestras APIs cuentan con el parametro `fields` para poder devolver un subconjunto de la respuesta original. Este `query parameter` permite recibir una lista separada por comas de los campos que se desean recibir.

Para aquellos casos donde la estructura del recurso (JSON) tenga varios niveles, el nombre de los campos deberá ser colocado en su forma plana, o sea separado cada nivel por un `.` (punto). Por ejemplo:

```
{
	atributo1Nivel0: {
		atributo1Nivel1: "valor"
	},
	atributo2Nivel0: "valor"
}
```

En este caso si nosotros quisieramos obtener el campo `atributo1Nivel1`, haríamos la petición de la siguiente forma: `?fields=atributo1Nivel0.atributo1Nivel1`

## Controles hipermedia

Algunas de nuestras APIs cuentan con controles hipermedia. Aquellos que te permitiran acceder a otros recursos relacionados. Por ejemplo la siguiente página de resultados. Por ejemplo:

```
{
	content: {
		price: "valor",
		...
	},
	page: {
		number: 1,
		total: 10,
		...
	}
	_links: {
		self: "...",
		next: "...",
		...
	}
}
```

En estos casos si nosotros quisieramos obtener únicamente el `price`, la forma correcta de solicitarlo es `fields=price` en lugar de `fields=content.price` pues el filtrado se lleva a cabo sobre el recurso no sobre los controles hipermedia. En ese caso el recurso se encuentra dentro de `content` y es donde se llevará a cabo el filtrado de campos.
