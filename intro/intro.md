# Introducción

El API de BIVA Cloud esta basado en REST con URLs orientadas a **recursos**. Las respuestas se encuentran en formato JSON y devuelven códigos de respuesta HTTP estándar.

Todas nuestras APIs las encontrarás en la siguiente URL base https://cloud.biva.mx. También podrás hacer uso de las APIs en el arenero en la siguiente URL base https://sandbox.biva.mx
