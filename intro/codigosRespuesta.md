# Códigos de respuesta

BIVA Cloud utiliza códigos de respuesta HTTP para indicar si una solicitud a una API fue exitosa o no.

En general podemos resumir que los códigos de respuesta son:

* 2xx Éxito.
* 4xx Si llegara a ocurrir algún error de acuerdo a la solicitud realizada.
* 5xx Si llegara a ocurrir algún error en los servicios de BIVA Cloud.

Ten en cuenta que todas las solicitudes 2xx y 4xx sumarán en el consumo de tu API Key. En caso de obtener cualquier código de respuesta 5xx, esta petición **no** incrementará tu consumo.

## Códigos de status HTTP de BIVA Cloud

| Código HTTP | Tipo          | Descripción |
| ----------- | ------------- | ----------- |
| 400         |               |             |
| 401         |               |             |
| 403         | Forbidden     | API Key invalido |
| 403         | Usage Limit Exceeded | Se han excedido la cantidad de peticiones contratadas a ese recurso |
| 404         | No encontrado | El recurso solicitado no fue encontrado |
| 500         | Error interno | Algo salió mal en nuestros servicios de BIVA Cloud |
