# Cotización

Devuelve la cotización de los instrumentos operados.

## Request

**GET** `/stock-exchange/{stock-exchange}/quote`

## Response

```
{
  "displayName": "WALMEX *",
  "key": "WALMEX *",
  "timeSeries": [
    {
      "dateInMillis": 1611239460000,
      "open": 62.35,
      "close": 62.69,
      "high": 62.69,
      "low": 62.33,
      "volume": 1734
    },
    {
      "dateInMillis": 1611239580000,
      "open": 62.74,
      "close": 62.74,
      "high": 62.74,
      "low": 62.74,
      "volume": 100
    }
  ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/stock-exchange/BIVA/quote?isin=MX01WA000038&period=D&quantity=1`

## Path variables

* **stock-exchange** Indica la bolsa de donde se obtiene la estadística. Posibles valores BIVA

## Query parameters

* **isin** Indica el ISIN del instrumento deseado **Requerido**
* **period**: Periodo a realizar la consulta. Posibles valores: H (Hora) , D (Día), M (Mes), Y (Año) **Requerido**
* **quantity**: Valor del periodo. Posibles valores: valores numéricos enteros. **Requerido**

## Atributos en la respuesta
