# Instrumentos listados

Devuelve los instrumentos que se encuentran listados.

## Request

**GET** `/instruments`

## Response

```
{
  "content": [
    {
      "securityName": "WALMEX *",
      "securityShortName": "WAL - MART DE MEXICO, S.A.B. DE C.V.",
      "isin": "MX01WA000038",
      "clasification": "LOCAL",
      "sectorName": "VENTA DE PRODUC DE CON FREC",
      "issuedQuantity": 17461402631,
      "listingDate": null,
      "delistingDateTime": null
    }
  ],
  "page": {
    "number": 0,
    "size": 1,
    "totalPages": 1,
    "totalElements": 1
  },
  "_links": [
    {
      "rel": "first",
      "href": "/instruments?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "prev",
      "href": "/instruments?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "self",
      "href": "/instruments?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "next",
      "href": "/instruments?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "last",
      "href": "/instruments?page=0&size=1&instrument=WALMEX *"
    }
  ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/instruments?page=0&size=1&instrument=WALMEX%20*`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. Si no se indica por default mostrará la página 0
* **size**: Indica el tamaño de la página, los registros a mostrar. Si no se indica por default tomará el tamaño máximo configurado en la aplicación.
* **isin**: Lista de ISINs a filtrar. Se deberán separar por coma.
* **instrument** Lista de nombre de cotización separadas por coma. Por ejemplo WALMEX *

## Atributos en la respuesta