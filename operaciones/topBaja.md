# Top de instrumentos por porcentaje a la baja

Devuelve el top de los instrumentos ordenados por porcentaje a la baja.

## Request

**GET** `/stock-exchange/{stock-exchange}/instruments/top/{top}/loss-percentage`

## Response

```
[
  {
    "instrument": "WALMEX *",
    "type": "equity",
    "volume": 857870,
    "amount": 48801531.4,
    "trades": 5617,
    "lastTradeTime": "1607741980095",
    "openPrice": null,
    "closePrice": 57,
    "high": 57.48,
    "low": 55.43,
    "gainLoss": 1.5,
    "gainLossPercentage": 2.7,
    "isin": "MX01WA000038"
  }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/stock-exchange/BIVA/instruments/top/10/loss-percentage`

## Path variables

* **stock-exchange** Indica la bolsa de donde se obtiene la estadística. Posibles valores BIVA
* **top** Indica la cantidad de elementos máximos a obtener en número entero. Por ejemplo 10

## Query parameters

N/A

## Atributos en la respuesta
