# Estadística de los instrumentos operados

Devuelve las estadísticas de los instrumentos operados.

## Request

**GET** `/stock-exchange/{stock-exchange}/instruments/stats`

## Response

```
{
  "content": [
    {
      "instrument": "WALMEX *",
      "type": "equity",
      "volume": 857870,
      "amount": 48801531.4,
      "trades": 5617,
      "lastTradeTime": "1607741980095",
      "openPrice": null,
      "closePrice": 57,
      "high": 57.48,
      "low": 55.43,
      "gainLoss": 1.5,
      "gainLossPercentage": 2.7,
      "isin": "MX01WA000038",
      "status": "active",
      "lastTradedPrice": 57,
      "bestBidPrice": 55.49,
      "bestBidVolume": 2102,
      "bestOfferPrice": 57.57,
      "bestOfferVolume": 80,
      "crossTotalVolume": null,
      "crossTotalValue": null,
      "numberCrosses": null,
      "exCrossTotalVolume": null,
      "exCrossTotalValue": null,
      "exNumberCrosses": null,
      "previousClosePrice": 55.5
    }
  ],
  "page": {
    "number": 0,
    "size": 1,
    "totalPages": 1,
    "totalElements": 1
  },
  "_links": [
    {
      "rel": "first",
      "href": "/stock-exchange/BIVA/instruments/stats?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "prev",
      "href": "/stock-exchange/BIVA/instruments/stats?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "self",
      "href": "/stock-exchange/BIVA/instruments/stats?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "next",
      "href": "/stock-exchange/BIVA/instruments/stats?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "last",
      "href": "/stock-exchange/BIVA/instruments/stats?page=0&size=1&instrument=WALMEX *"
    }
  ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/stock-exchange/BIVA/instruments/stats?page=0&size=1`

## Path variables

* **stock-exchange** Indica la bolsa de donde se obtiene la estadística. Posibles valores BIVA

## Query parameters

* **page**: Indica la página a mostrar. Si no se indica por default mostrará la página 0
* **size**: Indica el tamaño de la página, los registros a mostrar. Si no se indica por default tomará el tamaño máximo configurado en la aplicación.
* **isin**: Lista de ISINs a filtrar. Se deberán separar por coma.
* **instrument** Lista de nombre de cotización separadas por coma. Por ejemplo WALMEX *

## Atributos en la respuesta
