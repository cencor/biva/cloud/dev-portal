# Participación de mercado.

Devuelve la participación de mercado entre BIVA y BMV

## Request

**GET** `/trading/market-share`

## Response

```
{
  "updatedTime": 1607964282000,
  "marketShare": [
    {
      "stockExchange": "BIVA",
      "amount": 1776950607.7,
      "amountPercentage": 50.0,
      "trade": 9453,
      "tradePercentage": 50.0,
      "volume": 3901527,
      "volumePercentage": 50.0
    },
    {
      "stockExchange": "BMV",
      "amount": 1776950607.7,
      "amountPercentage": 50.0,
      "trade": 9453,
      "tradePercentage": 50.0,
      "volume": 3901527,
      "volumePercentage": 50.0
    }
  ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/trading/market-share`

## Path variables

N/A

## Query parameters

N/A

## Atributos en la respuesta

* **stockExchange** Indica la bolsa a la que pertenecen los siguientes atributos.
* **amount** El importe operado por la bolsa.
* **amountPercentage** El porcentaje del importe operado por la bolsa
* **trade** La cantidad de trades realizados por la bolsa
* **tradePercentage** El porcentaje de los trades realizados por la bolsa
* **volume** El volumen operado por la bolsa
* **volumePercentage** El porcentaje del volumen operado por la bolsa
