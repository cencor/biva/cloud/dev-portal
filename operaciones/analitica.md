# Analítica de los instrumentos operados

Devuelve los analíticos de los instrumentos operados.

## Request

**GET** `/stock-exchange/{stock-exchange}/instruments/analytics`

## Response

```
{
  "content": [
    {
      "isin": "MX01WA000038",
      "toRatio3Mth": 0.0012205,
      "toRatioYTD": 0.0036247,
      "toRatio2YR": 0.0072319
    }
  ],
  "page": {
    "number": 0,
    "size": 1,
    "totalPages": 1,
    "totalElements": 1
  },
  "_links": [
    {
      "rel": "first",
      "href": "/stock-exchange/BIVA/instruments/analytics?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "prev",
      "href": "/stock-exchange/BIVA/instruments/analytics?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "self",
      "href": "/stock-exchange/BIVA/instruments/analytics?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "next",
      "href": "/stock-exchange/BIVA/instruments/analytics?page=0&size=1&instrument=WALMEX *"
    },
    {
      "rel": "last",
      "href": "/stock-exchange/BIVA/instruments/analytics?page=0&size=1&instrument=WALMEX *"
    }
  ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/stock-exchange/BIVA/instruments/analytics?page=0&size=1`

## Path variables

* **stock-exchange** Indica la bolsa de donde se obtiene la estadística. Posibles valores BIVA

## Query parameters

* **page**: Indica la página a mostrar. Si no se indica por default mostrará la página 0
* **size**: Indica el tamaño de la página, los registros a mostrar. Si no se indica por default tomará el tamaño máximo configurado en la aplicación.
* **isin**: Lista de ISINs a filtrar. Se deberán separar por coma.
* **instrument** Lista de nombre de cotización separadas por coma. Por ejemplo WALMEX *

## Atributos en la respuesta
