# Clasificación de fondos listados

Devuelve catálogo de las clasificaciones de fondos listados.

## Request

**GET** `/emisoras/fondos/clasificacion-fondo`

## Response

```
[
    {
        "id": 141,
        "nombre": "Corto plazo gubernamental"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/clasificacion-fondo?idOperadora=3505&idFondo=3509&tipo=59`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.

## Atributos en la respuesta