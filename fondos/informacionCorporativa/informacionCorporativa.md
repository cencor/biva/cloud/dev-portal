# Información corporativa de fondos

Devuelve los documentos publicados por las operadoras de fondos.

## Request

**GET** `/emisoras/fondos/informacion-corporativa`

## Response

```
{
    "content": [
        {
            "id": 14449,
            "idDocumento": 14449,
            "claveOperadora": "MASFOND",
            "claveFondo": "AZMT-G1",
            "fechaPublicacion": 1620229297000,
            "descripcion": "Información para el público inversionista Abril-2021",
            "docType": "PDF",
            "periodo": "Abril",
            "ejercicio": 2021,
            "nombreArchivo": "/storage/data-maintenance/bivaqa2_42743.pdf"
        },
        ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 171,
    "totalPages": 18
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/informacion-corporativa?size=10&page=0&idOperadora=3505&idFondo=3508&tipoDocumento=164&periodo=4&ejercicio=2017`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipoDocumento**: Identificador tipo de fondo.
* **periodo** El periodo al que pertenece el documento. En caso de ser un tipo de documento mensual, formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], si es un documento trimestral ["1","2","3","4"].
* **ejercicio** El año al que pertenece el documento en formato yyyy
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta