# Tipo de documento de Información corporativa

Devuelve el nombre y id de los tipos de documento de los documentos publicados por las operadoras de fondos.

## Request

**GET** `/emisoras/fondos/informacion-corporativa/tipo-documento`

## Response

```
[
    {
        "id": "89,38",
        "nombre": "Eventos relevantes"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/informacion-corporativa/tipo-documento`

## Path variables

N/A

## Query parameters

N/A

## Atributos en la respuesta