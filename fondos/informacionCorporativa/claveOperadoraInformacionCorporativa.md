# Clave de operadoras para Información corporativa

Devuelve el catálogo de las operadoras disponibles para consultar su información corporativa.

## Request

**GET** `/emisoras/fondos/informacion-corporativa/clave-operadora`

## Response

```
[
    {
        "id": 3505,
        "nombre": "MASFOND"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/informacion-corporativa/clave-operadora`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **tipoDocumento**: Identificador tipo de fondo.
* **periodo** El periodo al que pertenece el documento. En caso de ser un tipo de documento mensual, formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], si es un documento trimestral ["1","2","3","4"].
* **ejercicio** El año al que pertenece el documento en formato yyyy
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta