# Periodos con Información corporativa

Devuelve catálogo de los periodos de documentos publicados por las operadoras de fondos.

## Request

**GET** `/emisoras/fondos/informacion-corporativa/periodo`

## Response

```
[
    {
        "id": "1",
        "nombre": "1"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/informacion-corporativa/periodo`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipoDocumento**: Identificador tipo de fondo.
* **ejercicio** El año al que pertenece el documento en formato yyyy
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta
