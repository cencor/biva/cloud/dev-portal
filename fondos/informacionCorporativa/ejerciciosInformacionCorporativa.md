# Ejercicios con Información corporativa

Devuelve catálogo de los ejercicios de documentos publicados por las operadoras de fondos.

## Request

**GET** `/emisoras/fondos/informacion-corporativa/ejercicio`

## Response

```
[
    {
        "id": "2021",
        "nombre": "2021"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/informacion-corporativa/ejercicio`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipoDocumento**: Identificador tipo de fondo.
* **periodo** El periodo al que pertenece el documento. En caso de ser un tipo de documento mensual, formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], si es un documento trimestral ["1","2","3","4"].
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta