# Claves de operadoras con información de precios

Devuelve clave e identificador de la operadora con información de precios.

## Request

**GET** `/emisoras/fondos/precios/clave-operadora`

## Response

```
[
    {
        "id": 3506,
        "nombre": "+FONDOS"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/clave-operadora?serie=0B,F1`

## Path variables

N/A

## Query parameters

* **serie**: Lista de series a filtrar. Se deberán separar por coma.
* **idFondo**: Identificador de fondo.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta