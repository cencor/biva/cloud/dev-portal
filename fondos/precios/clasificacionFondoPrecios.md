# Clasificaciones de fondos con información de precios

Devuelve catálogo de las clasificaciones de fondos con información de precios.

## Request

**GET** `/emisoras/fondos/precios/clasificacion-fondo`

## Response

```
[
    {
        "id": 141,
        "nombre": "Corto plazo gubernamental"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/clasificacion-fondo?idOperadora=3506`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.

## Atributos en la respuesta