# Claves de fondos con información de precios

Devuelve clave e identificador del fondo con información de precios.

## Request

**GET** `/emisoras/fondos/precios/clave-fondo`

## Response

```
[
    {
        "id": 3507,
        "nombre": "AZMT-C1"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/clave-fondo?tipo=62&clasificacion=152`

## Path variables

N/A

## Query parameters

* **serie**: Lista de series a filtrar. Se deberán separar por coma.
* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta