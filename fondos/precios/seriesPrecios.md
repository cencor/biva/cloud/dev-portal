# Series de fondos con información de precios

Devuelve las series que tienen los fondos con información de precios.

## Request

**GET** `/emisoras/fondos/precios/serie`

## Response

```
[
    {
        "id": "0A",
        "nombre": "0A"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/serie?tipo=62&clasificacion=152`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta