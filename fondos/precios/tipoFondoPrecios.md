# Tipos de fondos con información de precios

Devuelve catálogo de los tipos de fondos con información de precios.

## Request

**GET** `/emisoras/fondos/precios/tipo-fondo`

## Response

```
[
    {
        "id": 59,
        "nombre": "Fondo de inversión de deuda"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/tipo-fondo?idOperadora=3506`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.

## Atributos en la respuesta