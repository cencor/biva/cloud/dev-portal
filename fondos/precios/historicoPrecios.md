# Histórico de precios por fondo y serie

Devuelve el histórico de precios por fondo y serie.

## Request

**GET** `/emisoras/fondos/precios/historico`

## Response

```
{
    "content": [
        {
            "serie": "A",
            "valorContable": 1.315377,
            "precioCompra": 1.315377,
            "operacionCompra": 0,
            "volumenCompra": 0,
            "precioVenta": 1.315377,
            "operacionVenta": 0,
            "volumenVenta": 0,
            "tipoValor": 51,
            "clave": "AZMT-C1",
            "fechaPrecio": 1619067600000,
            "fechaPublicacion": 1619112660000
        }
    ],
    "number": 0,
    "size": 1,
    "totalElements": 35,
    "totalPages": 35
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/precios/historico?size=10&page=0&idOperadora=3505&idFondo=3508&tipo=62&clasificacion=152&serie=A&fechaInicio=2021-04-20&fechaFin=2021-04-22`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar.
* **size**: Indica el tamaño de la página.
* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.
* **serie**: Lista de series a filtrar. Se deberán separar por coma.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta