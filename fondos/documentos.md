# Documento DICI y Prospecto de fondo listado

Devuelve el documento DICI y Prospecto del fondo listado en BIVA.

## Request

**GET** `/emisoras/prospectos/{idFondo}/documentos-fondo`

## Response

```
[
    {
        "idPublicacion": 14606,
        "fechaPublicacion": 1620412426000,
        "urlDocumento": "/storage/data-maintenance/bivaqa2_43849.pdf",
        "tipoDocumentoListado": {
            "id": 195,
            "nombre": ""
        },
        "docType": "PDF"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/prospectos/3507/documentos-fondo`

## Path variables

* **idFondo** Identificador del fondo a consultar.

## Query parameters

N/A

## Atributos en la respuesta