# Clave de fondos listados

Devuelve clave e identificador del fondo listado en BIVA.

## Request

**GET** `/emisoras/fondos/clave-fondo`

## Response

```
[
    {
        "id": 3606,
        "nombre": "AZMT-G+"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/clave-fondo?idOperadora=3505&tipo=60&clasificacion=143`

## Path variables

N/A

## Query parameters

* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.

## Atributos en la respuesta
