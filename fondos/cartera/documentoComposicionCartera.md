# Documento de Composición de cartera de Fondos

Devuelve el documento de la composición de cartera para el fondo indicado.

## Request

**GET** `/emisoras/fondos/composicion-cartera/archive`

## Response

```
{
    "url": "/storage/data-maintenance/bivaqa2_42659.csv",
    "periodo": "Abril",
    "ejercicio": 2021
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/composicion-cartera/archive?idOperadora=3505&idFondo=3507`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo. **Requerido**
* **idOperadora**: Identificador de operadora. **Requerido**

## Atributos en la respuesta
