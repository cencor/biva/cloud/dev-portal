# Claves de Fondos para composición de cartera

Devuelve el catálogo de los fondos disponibles para consultar su composición de cartera.

## Request

**GET** `/emisoras/fondos/composicion-cartera/clave-fondo`

## Response

```
[
    {
        "id": 3507,
        "nombre": "AZMT-C1"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/composicion-cartera/clave-fondo?idOperadora=3505`

## Path variables

N/A

## Query parameters

* **idOperadora**: Identificador de operadora.

## Atributos en la respuesta