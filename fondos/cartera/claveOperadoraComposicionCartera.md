# Claves de Operadoras para composición de cartera

Devuelve el catálogo de las operadoras disponibles para consultar su composición de cartera.

## Request

**GET** `/emisoras/fondos/composicion-cartera/clave-operadora`

## Response

```
[
    {
        "id": 3505,
        "nombre": "MASFOND"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/composicion-cartera/clave-operadora`

## Path variables

N/A

## Query parameters

N/A

## Atributos en la respuesta