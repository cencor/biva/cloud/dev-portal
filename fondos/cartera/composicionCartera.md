# Composición de cartera de Fondos

Devuelve la composición de cartera para el fondo indicado.

## Request

**GET** `/emisoras/fondos/composicion-cartera`

## Response

```
{
    "periodo": "Enero",
    "ejercicio": 2021,
    "activosFinancieros": [
        {
            "emisora": "ACBE",
            "serie": "17-2",
            "tipoValor": "91",
            "titulosOperados": 177958,
            "titulosEnCirculacion": 70000000,
            "precioPromedio": 100.121826,
            "valorRazonable": 100.123072,
            "diasPorVencer": 891
        },
        ...
    ]
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/composicion-cartera?idOperadora=3505&idFondo=3507`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo. **Requerido**
* **idOperadora**: Identificador de operadora. **Requerido**

## Atributos en la respuesta