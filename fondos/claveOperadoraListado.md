# Clave de operadoras con fondos listados

Devuelve clave e identificador de las operadoras con fondos listados.

## Request

**GET** `/emisoras/fondos/clave-operadora`

## Response

```
[
    {
        "id": 3505,
        "nombre": "MASFOND"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/clave-operadora?idFondo=3509&tipo=59&clasificacion=141`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.

## Atributos en la respuesta