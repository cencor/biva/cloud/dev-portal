# Listado de fondos

Devuelve el listado de fondos listados en BIVA.

## Request

**GET** `/emisoras/fondos`

## Response

```
{
    "content": [
        {
            "idFondo": 3507,
            "claveFondo": "AZMT-C1",
            "claveOperadora": "MASFOND"
        },
        ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 5,
    "totalPages": 1
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos?page=0&size=10`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **tipo**: Identificador tipo de fondo.
* **clasificacion**: Identificador clasificación de fondo.

## Atributos en la respuesta