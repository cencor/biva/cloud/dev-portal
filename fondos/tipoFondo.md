# Tipo de fondos listados

Devuelve catálogo de los tipos de fondos listados.

## Request

**GET** `/emisoras/fondos/tipo-fondo`

## Response

```
[
    {
        "id": 59,
        "nombre": "Fondo de inversión de deuda"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/tipo-fondo?idOperadora=3505&idFondo=5986&clasificacion=206`

## Path variables

N/A

## Query parameters

* **idFondo**: Identificador de fondo.
* **idOperadora**: Identificador de operadora.
* **clasificacion**: Identificador clasificación de fondo.

## Atributos en la respuesta
