# Características de fondo listado

Devuelve información general de un fondo listado en BIVA.

## Request

**GET** `/emisoras/fondos/{idFondo}`

## Response

```
{
    "razonSocial": "MÁS FONDOS MEDIANO PLAZO, S.A. DE C.V. FONDO DE INVERSIÓN EN INSTRUMENTOS DE DEUDA",
    "tipo": "Fondo de inversión de deuda",
    "clasificacion": "Mediano plazo general",
    "series": [
        "A",
        "E1",
        ...
    ],
    "emisora": {
        "razonSocial": "MÁS FONDOS, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN",
        "direccion": "Montes Urales #505 piso 2, Lomas de Chapultepec, Miguel Hidalgo, CDMX, CP. 11000",
        "sitioWeb": "http://masfondos.mx/",
        "contacto": "Rubén Pérez Bravo"
    },
    "calificacion": "FITCH: AAAf(mex)"
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/fondos/3507`

## Path variables

* **idFondo** Identificador del fondo a consultar.

## Query parameters

N/A

## Atributos en la respuesta

* **emisora** Contiene los datos de contacto de la operadora.