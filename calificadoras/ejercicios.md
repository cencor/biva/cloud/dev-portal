# Ejercicios con Reportes mensuales.

Devuelve el catálogo de ejercicios con reportes mensuales para las Calificadoras.

## Request

**GET** `/emisoras/empresas/calificadoras/{idCalificadora}/ejercicio`

## Response

```
[
    2021,
    2020
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/calificadoras/21/ejercicio`

## Path variables

* **idCalificadora**: Identificador de la calificadora.

## Query parameters

* **periodo**: El periodo al que pertenece el documento. Formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]

## Atributos en la respuesta