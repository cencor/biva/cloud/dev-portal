# Reporte mensual de calificadoras.

Devuelve los Reportes mensuales reportados por las Calificadoras.

## Request

**GET** `/emisoras/empresas/calificadoras/{idCalificadora}/reportes`

## Response

```
{
    "content":
    [
        {
            "id": 15428,
            "clave": "AMBEST",
            "serie": null,
            "fechaPublicacion": 1622852136000,
            "fechaCreacion": 1622852136000,
            "docType": "XLS",
            "tipoDocumento": "Reporte de calificaciones Mayo-2021",
            "nombreArchivo": "/storage/data-maintenance/bivaqa2_49631.xls"
        },
       ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 11,
    "totalPages": 2
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/calificadoras/21/reportes?size=10&page=0`

## Path variables

* **idCalificadora**: Identificador de la calificadora.

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **periodo**: El periodo al que pertenece el documento. Formato nombre del mes en español iniciando en mayúsculas. ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
* **ejercicio**: El año al que pertenece el documento en formato yyyy.

## Atributos en la respuesta

* **id** El identificador del documento.
* **clave** La clave de la calificadora que envió el documento.
* **serie** Serie a la que está asociada el documento.
* **fechaPublicacion** La fecha de publicación del documento en formato EPOCH.
* **docType** La extensión del documento
* **tipoDocumento** La descripción del tipo de documento
* **nombreArchivo** La ruta relativa a https://www.biva.mx donde se encuentra el documento.