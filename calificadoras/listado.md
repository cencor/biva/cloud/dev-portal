# Listado de calificadoras.

Devuelve el listado de las Calificadoras.

## Request

**GET** `/emisoras/empresas/calificadoras`

## Response

```
{
    "content": [
        {
            "id": 21,
            "nombre": "A.M. BEST AMERICA LATINA, S.A DE C.V., INSTITUCIÓN CALIFICADORA DE VALORES",
            "rutaLogo": "https://jupiterbootstoacc01.blob.core.windows.net/data-maintenance/biva_21.gif"
        },
        {
            "id": 18,
            "nombre": "FITCH MEXICO S.A. DE C.V.",
            "rutaLogo": "https://jupiterbootstoacc01.blob.core.windows.net/data-maintenance/biva_18.png"
        },
        ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 6,
    "totalPages": 1
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/calificadoras?size=10&page=0`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**

## Atributos en la respuesta