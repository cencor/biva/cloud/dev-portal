# Periodos con Reportes mensuales.

Devuelve el catálogo de periodos con reportes mensuales para las Calificadoras.

## Request

**GET** `/emisoras/empresas/calificadoras/{idCalificadora}/periodo`

## Response

```
[
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/calificadoras/21/periodo`

## Path variables

* **idCalificadora**: Identificador de la calificadora.

## Query parameters

N/A

## Atributos en la respuesta