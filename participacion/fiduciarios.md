# Participación de fiduciarios por emisión

Devuelve relacion entre emisiones y su fiduciario participante.

## Request

**GET** `/emisoras/empresas/participantes/fiduciario`

## Response

```
{
    "content": [
        {
            "id": 3229,
            "clave": "123LCB",
            "serie": "19",
            "tipoValor": "91",
            "fiduciario": "FACTIN",
            "emisora": "FACTIN",
            "fechaListado": "2019-10-10",
            "fechaVencimiento": "2023-10-10"
        }
    ],
    "number": 0,
    "size": 1,
    "totalElements": 51,
    "totalPages": 51
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/participantes/fiduciario?size=10&page=0&empresa=11`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar.
* **size**: Indica el tamaño de la página, los registros a mostrar.
* **empresa**: El identificador del fiduciario.

## Atributos en la respuesta
