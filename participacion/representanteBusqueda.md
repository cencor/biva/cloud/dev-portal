# Búsqueda predictiva de participantes representante común por razón social

Devuelve clave e identificador del representante común.

## Request

**GET** `/emisoras/empresas/participantes/representante-comun/busqueda/{razon-social}`

## Response

```
[
    {
        "id": 38,
        "clave": "MONEX CASA DE BOLSA, S.A. DE C.V."
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/participantes/representante-comun/busqueda/MONEX`

## Path variables

* **razon-social** Indica la razón social del fiduciario a buscar, permite desde un solo carácter.

## Query parameters

N/A

## Atributos en la respuesta