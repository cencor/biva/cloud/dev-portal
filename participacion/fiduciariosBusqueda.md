# Búsqueda predictiva de participantes fiduciarios por razón social

Devuelve clave e identificador del fiduciario.

## Request

**GET** `/emisoras/empresas/participantes/fiduciario/busqueda/{razon-social}`

## Response

```
[
    {
        "id": 11,
        "clave": "CIBANCO, S.A., INSTITUCIÓN DE BANCA MÚLTIPLE"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/participantes/fiduciario/busqueda/CIB`

## Path variables

* **razon-social** Indica la razón social del fiduciario a buscar, permite desde un solo carácter.

## Query parameters

N/A

## Atributos en la respuesta