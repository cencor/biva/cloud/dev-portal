# Participación de representante común por emisión

Devuelve relación entre emisiones y su representante común participante.

## Request

**GET** `/emisoras/empresas/participantes/representante-comun`

## Response

```
{
    "content": [
        {
            "id": 3229,
            "clave": "123LCB",
            "serie": "19",
            "tipoValor": "91",
            "representanteComun": "MNXCB",
            "razonSocial": "BANCO ACTINVER, S.A. INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO ACTINVER",
            "emisora": "FACTIN",
            "fechaListado": "2019-10-10",
            "fechaVencimiento": "2023-10-10"
        }
    ],
    "number": 0,
    "size": 1,
    "totalElements": 135,
    "totalPages": 135
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/participantes/representante-comun?page=0&size=10&empresa=38`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar.
* **size**: Indica el tamaño de la página, los registros a mostrar.
* **empresa**: El identificador del representante común.

## Atributos en la respuesta