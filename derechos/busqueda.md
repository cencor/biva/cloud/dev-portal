# Búsqueda de claves de cotización para Agenda de derechos

Devuelve clave de cotización e identificador.

## Request

**GET** `/derechos/busqueda/{cadenaBusqueda}`

## Response

```
[
    {
        "0": 0,
        "1": "CADU"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/derechos/busqueda/ca?nacional=true`

## Path variables

* **cadenaBusqueda**: Clave de cotización a buscar, permite desde un solo carácter.

## Query parameters

* **nacional**: Indica que solo mostrará agendas de derecho nacionales. **Requerido este parámetro o sic**
* **sic**: Indica que solo mostrará agendas de derecho internacionales. **Requerido este parámetro o nacional**

## Atributos en la respuesta