# Estatus de Agenda de derechos

Devuelve el catálogo de posibles estatus de las agendas de derechos.

## Request

**GET** `/derechos/estatus`

## Response

```
[
    {
        "id": 2,
        "nombre": "Aplicado"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/derechos/estatus`

## Path variables

N/A

## Query parameters

* **nacional**: Indica que solo mostrará los estatus de agendas de derecho nacionales, si no se envía este parámetro retorna los estatus de agendas de derecho internacionales.

## Atributos en la respuesta