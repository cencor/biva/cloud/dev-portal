# Tipos de Instrumento para Agenda de derechos

Devuelve el catálogo de tipos de instrumento de las agendas de derechos.

## Request

**GET** `/derechos/tipo-instrumento`

## Response

```
[
    {
        "id": 1,
        "nombre": "Acciones, CPOs, UVs"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/derechos/tipo-instrumento?nacional=true`

## Path variables

N/A

## Query parameters

* **nacional**: Indica que solo mostrará agendas de derecho nacionales. **Requerido este parámetro o sic**
* **sic**: Indica que solo mostrará agendas de derecho internacionales. **Requerido este parámetro o nacional**
* **tipoDerecho** Identificador del tipo de derecho.
* **estatus** Identificador del estatus.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta