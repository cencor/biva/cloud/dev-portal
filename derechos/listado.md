# Agenda de derechos

Devuelve el listado de las agendas de derechos.

## Request

**GET** `/derechos`

## Response

```
{
    "content": [
        {
            "id": 102,
            "clave": "CKD32",
            "estatus": "Pagado",
            "idEstatus": 3,
            "serie": "YTR",
            "fechaEx": 1605679200000,
            "fechaRegistro": 1605765600000,
            "fechaPago": 1605852000000,
            "fechaModificacion": null,
            "idTipoDerecho": 13,
            "tipoDerecho": "Distribución en efectivo",
            "idTipoInstrumento": 3,
            "proporcion": "100 EUR",
            "tipoAgenda": "agenda_derecho",
            "idTipoAgenda": 1,
            "accionesActuales": null,
            "accionesNuevas": null,
            "precioSuscripcion": null,
            "importeTituloProporcion": null,
            "claveTipoValor": "1R",
            "valoresAEmitir": null,
            "codigoMoneda": "EUR",
            "distribucionPorTitulo": 100.000000000000000,
            "precioPorTitulo": null,
            "emision": "1R_CKD32_YTR",
            "isin": null,
            "idTipoInstrumentoAlternativo": null
        },
        ...
    ],
    "totalElements": 592,
    "totalPages": 60,
    "number": 0,
    "size": 10
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/derechos?nacional=true&page=0&size=10&fechaInicio=2020-11-18&fechaFin=2021-05-18`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **nacional**: Indica que solo mostrará agendas de derecho nacionales. **Requerido este parámetro o sic**
* **sic**: Indica que solo mostrará agendas de derecho internacionales. **Requerido este parámetro o nacional**
* **tipoInstrumento** Identificador del tipo de instrumento.
* **tipoDerecho** Identificador del tipo de derecho.
* **estatus** Identificador del estatus.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

## Atributos en la respuesta