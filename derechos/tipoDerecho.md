# Tipos de Derecho para Agenda de derechos

Devuelve el catálogo de tipos de derechos de las agendas de derechos.

#### Request

**GET** `/derechos/tipo-derecho`

#### Response

```
[
    {
        "id": 7,
        "nombre": "Canje simple"
    },
    ...
]
```
#### Sincronización de información

Tiempo real

#### Ejemplo

**GET** `/derechos/tipo-derecho?sic=true`

#### Path variables

N/A

#### Query parameters

* **nacional**: Indica que solo mostrará agendas de derecho nacionales. **Requerido este parámetro o sic**
* **sic**: Indica que solo mostrará agendas de derecho internacionales. **Requerido este parámetro o nacional**
* **tipoInstrumento** Identificador del tipo de instrumento.
* **estatus** Identificador del estatus.
* **fechaInicio** La fecha de inicio en formato yyyy-MM-dd
* **fechaFin** La fecha de fin en formato yyyy-MM-dd

#### Atributos en la respuesta