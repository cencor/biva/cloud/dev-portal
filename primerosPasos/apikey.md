# Obtener un API Key

Para usar cualquiera de nuestras APIs debes solicitar un API Key y una cuenta de desarrollador. La cuenta de desarrollador proporciona tu API Key para acceder a nuestras APIs, una zona donde puedes probar nuestras APIs así como las métricas de consumo.

Tu API Key la puedes solicitar al teléfono + 52 (55) 4166 3333 o por correo electrónico a la dirección marketops@biva.mx

Dependiendo de la configuración de tu cuenta podrás tener acceso a una o varias de nuestras APIs. Estas APIs pueden estar relacionadas a tu plan de uso. Adicionalmente cualquier solicitud que hagas será contada en tu uso.

Sabemos que entender cómo usar nuestras APIs puede requerir algo de tu atención. Cada una de nuestras APIs están documentadas de tal forma que te ayuden a entender que parámetros puedes enviar y que respuesta vas a obtener. La mejor forma de madurar el entendimiento es probando el endpoint. Realiza una llamada a nuestro API utilizando tu API Key con `curl` con todos los parámetros necesarios y podrás obtener la respuesta real, jugar y entender cada una de las APIs.
