# Microsoft Excel BIVA Addin

BIVA Cloud proporciona un addin para Microsoft Excel donde puedes disfrutar de la información de mercado en tu hoja de cálculo.

Bastará con que accedas a la tienda de Microsoft, buscar BIVAddin e instalarlo. A continuación lo describimos a detalle.

1. Una vez abierto Microsoft Excel, vamos a la pestaña **Insertar**
2. A continuación demos click sobre **Obtener complementos**
3. En seguida se abrirá una ventana donde buscaremos **BIVAddin**
4. Ya que hayamos encontrado el complemento de BIVA daremos click en **Agregar**
5. Finalmente se mostrará el addin de BIVA del lado derecho de la pantalla de Microsoft Excel.

Una vez instalado el BIVAddin te solicitará tu API Key para poder llevar a cabo las consultas. Además te permitirá seleccionar las cotizaciones en las que estás interesado y poder establecer una actualización automática cada cierto tiempo.
