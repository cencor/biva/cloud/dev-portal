# cURL

Implementar un cliente Java te tomará definitivamente mucho más tiempo que una petición con `curl`.

Vamos a una terminal para mandar a llamar un API de BIVA Cloud. Coloca el siguiente comando en tu terminal:

```
curl -X GET "https://cloud.biva.mx/stock-exchange/BIVA/instruments/stats" -H "x-api-key: COLOCA_AQUI_TU_API_KEY"
```

¡Así de fácil! Definitivamente mucho más rápido que crear un cliente en Java para solo explorar un API, ¿No lo crees?
