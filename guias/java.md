# Java

Hacer tu primer cliente Java de BIVA Cloud es fácil! Hemos creado un cliente REST en Java al cual puedes hacer referencia.

Necesitarás:

* Java 11
* El IDE de tu preferencia
* Gradle 6+
* Tu API Key (Opcional si es que **simulas** las respuestas)

A continuación presentamos la implementación de un cliente REST, apoyándonos de Spring boot para hacer lo más claro posible:

```
public InstrumentStats getInstrumentStats(final String instrumentName) {
        HttpHeaders bivaCloudHeaders = new HttpHeaders();
        bivaCloudHeaders.set("x-api-key", apiKey);
        
        HttpEntity<Object> requestEntity = new HttpEntity<>(bivaCloudHeaders);
        
        String url = domain + "/stock-exchange/{stock-exchange}/instruments/stats?instrument={instrument}";
        
        String stockExchange = "BIVA";
        ResponseEntity<BivaCloudHypermediaControls> response = null;
        try {
            response = restTemplate.exchange(url,
                // Method
                HttpMethod.GET,
                // Request entity with headers.
                requestEntity,
                // Response Type
                BivaCloudHypermediaControls.class,
                // Path variable
                stockExchange, 
                // Query param
                instrumentName);
        } catch (HttpClientErrorException e) {
            // TODO: handle bad request exception
            LOGGER.error("Ocurrió un error al solicitar las estadísticas o el instrumento {} no existe.", 
                    instrumentName, e);
            /*
             * TODO Preferir el patron NullObjectPattern. Quizás sea más conveniente lanzar
             * una Runtime exception. Se devuelve null para efectos ilustrativos.
             */
            return null;
        } catch (HttpServerErrorException e) {
            // TODO: handle exception in case BIVA Cloud has an error or is not available.
            LOGGER.warn("Ocurrió un error del lado de BIVA Cloud...");
            /*
             * TODO Preferir el patron NullObjectPattern. Quizás sea más conveniente lanzar
             * una Runtime exception. Se devuelve null para efectos ilustrativos.
             */
            return null;
        }
        
        /*
         * Al buscar por nombre de instrumento se busca por nombre exacto. Por lo tanto
         * no puede haber más de un resultado.
         * 
         * En caso de no haber resultados provocaría una HttpClientErrorException que es
         * manejada anteriormente. Por lo tanto en este punto es seguro leer el dato que
         * solicitamos.
         */
        return response.getBody().getContent().get(0);
    }
```

Para más detalle sobre las dependencias utilizadas, cómo ejecutar pruebas con JUnit y el código completo de esta implementación lo puedes encontrar en el repositorio publicado en Gitlab: https://gitlab.com/cencor/biva/biva-cloud-rest-client

Siéntete libre de clonar o __forkear__ el repositorio, familiarizarte e inclusive tomar partes del código para tu implementación. Lo hemos puesto ahí para ese propósito.