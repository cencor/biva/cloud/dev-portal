# Perfil de la emisora nacional o SIC

Perfil de la emisora nacional o SIC especificada.

## Request

**GET** `/emisoras/empresas/{id_emisora}`

## Response

```
{
    "id": 1916,
    "logo": "https://jupiterbootstoacc01.blob.core.windows.net/data-maintenance/biva_1916.jpg",
    "bolsa": "BIVA",
    "direccion": "Paseo de la Reforma #510, Col. Juárez, Cuauhtémoc, Ciudad de México, México, CP. 06600",
    "razonSocial": "BBVA BANCOMER, S.A., INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO BBVA BANCOMER",
    "clave": "BACOMER",
    "estatus": "Activa",
    "fechaListado": 1556168400000,
    "telefono": "+52 (55) 5621 3434",
    "sitioWeb": "https://www.bancomer.com/",
    "sector": {
        "id": 7,
        "nombre": "Servicios financieros"
    },
    "subsector": {
        "id": 16,
        "nombre": "Entidades financieras"
    },
    "ramo": {
        "id": 50,
        "nombre": "Bancos"
    },
    "subramo": {
        "id": 112,
        "nombre": "Bancos comerciales"
    },
    "actividadEconomica": "Las que autoriza el artículo 46 de la Ley de Instituciones de Crédito"
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/1916`

## Path variables

* **id_emisora** El identificador de la emisora

## Query parameters

N/A

## Atributos en la respuesta