# Ramo para emisoras nacionales

Devuelve los ramos para emisoras nacionales registradas.

## Request

**GET** `/emisoras/sectores/{id_sector}/subsectores/{id_subsector}/ramos`

## Response

```
[
    {
        "id": 9,
        "nombre": "Fabricación y comercialización de materiales"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sectores/2/subsectores/2/ramos`

## Path variables

* **id_sector** El identificador del sector
* **id_subsector** El identificador del subsector

## Query parameters

* **tipoInstrumento** El identificador del tipo de instrumento
* **tipoValor** El identificador del tipo de valor
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta
