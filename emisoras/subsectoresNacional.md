# Subsectores para emisoras nacionales

Devuelve los sectores para emisoras nacionales registradas.

## Request

**GET** `/emisoras/sectores/{id_sector}/subsectores`

## Response

```
[
    {
        "id": 2,
        "nombre": "Materiales"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sectores/7/subsectores`

## Path variables

* **id_sector** El identificador del sector

## Query parameters

* **tipoInstrumento** El identificador del tipo de instrumento
* **tipoValor** El identificador del tipo de valor
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta