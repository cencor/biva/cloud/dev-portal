# Subramo para emisoras nacionales

Devuelve los subramos para emisoras nacionales registradas.

## Request

**GET** `/emisoras/sectores/{id_sector}/subsectores/{id_subsector}/ramos/{id_ramo}/subramos`

## Response

```
[
    {
        "id": 26,
        "nombre": "Procesamiento y distribución de productos de acero"
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sectores/2/subsectores/2/ramos/9/subramos`

## Path variables

* **id_sector** El identificador del sector
* **id_subsector** El identificador del subsector
* **id_ramo** El identificador del ramo

## Query parameters

* **tipoInstrumento** El identificador del tipo de instrumento
* **tipoValor** El identificador del tipo de valor
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta