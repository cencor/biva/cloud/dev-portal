# Tipo instrumento para emisoras nacionales

Devuelve los tipos de instrumento para emisoras nacionales registradas.

## Request

**GET** `/emisoras/tipo-instrumento`

## Response

```
[
    {
        "id": 1,
        "nombre": "Acciones, CPOs, UVs",
        "mercado": 1
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/tipo-instrumento`

## Path variables

N/A

## Query parameters

* **tipoValor** El identificador del tipo de valor
* **subramo** El identificador del subramo
* **ramo** El identificador del ramo
* **subsector** El identificador del subsector
* **sector** El identificador del sector
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta
