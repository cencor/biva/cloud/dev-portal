# Sectores para emisoras nacionales

Devuelve los sectores para emisoras nacionales registradas.

## Request

**GET** `/emisoras/sectores`

## Response

```
[
    {
        "id": 2,
        "nombre": "Materiales"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/sectores`

## Path variables

N/A

## Query parameters

* **tipoInstrumento** El identificador del tipo de instrumento
* **tipoValor** El identificador del tipo de valor
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta