# Tipo valor para emisoras nacionales

Devuelve los tipos de valor para emisoras nacionales registradas.

## Request

**GET** `/emisoras/tipo-instrumento/{id_tipo_instrumento}/tipo-valor`

## Response

```
[
    {
        "id": 10,
        "nombre": "FIBRAS CERTIFICADOS INMOBILIARIOS",
        "tipoInstrumento": 2
    },
    ...
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/tipo-instrumento/2/tipo-valor`

## Path variables

* **id_tipo_instrumento** El identificador del tipo de instrumento

## Query parameters

* **subramo** El identificador del subramo
* **ramo** El identificador del ramo
* **subsector** El identificador del subsector
* **sector** El identificador del sector
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta