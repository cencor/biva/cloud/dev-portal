# Listado de emisoras nacionales

Devuelve las emisoras nacionales registradas.

## Request

**GET** `/emisoras/empresas`

## Response

```
{
    "content": [
        {
            "id": 3229,
            "clave": "123LCB",
            "nombre": "BANCO ACTINVER, S.A. INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO ACTINVER"
        }
    ],
    "number": 0,
    "size": 1,
    "totalElements": 341,
    "totalPages": 341
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/empresas?size=10&page=0&tipoInstrumento=20&sector=7&subsector=16&biva=true`

## Path variables

N/A

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**
* **tipoInstrumento** El identificador del tipo de instrumento
* **tipoValor** El identificador del tipo de valor
* **subramo** El identificador del subramo
* **ramo** El identificador del ramo
* **subsector** El identificador del subsector
* **sector** El identificador del sector
* **biva** Boolean, indica si sólo mostrará resultados para emisoras listadas en BIVA
* **canceladas** Boolean, indica si sólo mostrará resultados para emisoras con estado "Cancelada"

## Atributos en la respuesta