# Valores listados de la emisora nacional

Presenta información de los valores listados para la emisora nacional especificada.

## Request

**GET** `/emisoras/empresas/{id_emisora}/emisiones`

## Response

```
{
    "content": [
        {
            "id": 10788,
            "serie": "DC005",
            "isin": "MXWAAA0G0007",
            "fechaEmision": 1622178000000,
            "tipoValor": "WARRANTS SOBRE ACCIONES",
            "claveTipoValor": "WA",
            "representanteComun": "MNXCB",
            "fechaVencimiento": 1669356000000,
            "claveWarrant": "AAG211L",
            "tipoInstrumento": "Warrants",
            "idTipoInstrumento": 5,
            "tipoEmision": "equity",
            "modoListado": "Registro Secundario",
            "idModoListado": 3,
            "nombre": "WARRANTS SOBRE ACCIONES AAG211L DC005"
        },
        ...
    ],
    "number": 0,
    "size": 10,
    "totalElements": 170,
    "totalPages": 17
}
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `/emisoras/empresas/1916/emisiones?size=10&page=0`

## Path variables

* **id_emisora** El identificador de la emisora

## Query parameters

* **page**: Indica la página a mostrar. **Requerido**
* **size**: Indica el tamaño de la página. **Requerido**

## Atributos en la respuesta