# Búsqueda de emisoras nacionales

Busca emisoras nacionales por su clave.

## Request

**GET** `emisoras/empresas/busqueda/{clave}`

## Response

```
[
    {
        "id": 1824,
        "clave": "BCCK"
    }
]
```
## Sincronización de información

Tiempo real

## Ejemplo

**GET** `emisoras/empresas/busqueda/BC`

## Path variables

* **clave**: Clave de la emisora a buscar, permite desde un solo carácter.

## Query parameters

N/A

## Atributos en la respuesta